const express = require('express');
const app = express();
const converter = require('./api/converter');

app.use('/api/converter', converter);

app.listen(process.env.PORT || 8080);
