function fromRoman(roman) {
  const romanNumList = [
    'CM',
    'M',
    'CD',
    'D',
    'XC',
    'C',
    'XL',
    'L',
    'IX',
    'X',
    'IV',
    'V',
    'I'
  ];
  const corresp = [900, 1000, 400, 500, 90, 100, 40, 50, 9, 10, 4, 5, 1];
  let index = 0,
    num = 0;
  for (let rn in romanNumList) {
    index = roman.indexOf(romanNumList[rn]);
    while (index !== -1) {
      num += parseInt(corresp[rn]);
      roman = roman.replace(romanNumList[rn], '-');
      index = roman.indexOf(romanNumList[rn]);
    }
  }
  return num;
}

module.exports = fromRoman;
