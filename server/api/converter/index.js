var express = require('express');
var router = express.Router();
var fromRoman = require('./fromRoman');
var toRoman = require('./toRoman');

router.get('/', function(req, res) {
  const { value } = req.query;

  if (value === undefined || value === '') {
    res.status(400).send({
      message: "value can't be empty"
    });
    return;
  }

  if (isNaN(value)) {
    if (
      !/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/.test(value)
    ) {
      res.status(400).send({
        message: 'value is not a valid roman numeral'
      });
      return;
    }

    res.json({
      value: `${fromRoman(value)}`,
      format: 'number'
    });
    return;
  } else {
    const number = +value;

    if (!Number.isInteger(number)) {
      res.status(400).send({
        message: 'value is not a valid whole number'
      });
      return;
    }

    if (number < 1 || number > 3999) {
      res.status(400).send({
        message: 'value must be in the range of 1 to 3999'
      });
      return;
    }

    res.json({
      value: `${toRoman(value)}`,
      format: 'roman numeral'
    });
  }
});

module.exports = router;
