import styled from 'styled-components';
import colours from './globals/colours';
import { lighten } from 'polished';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  background: ${colours.mintCream};
`;

export const HeaderContainer = styled.div`
  display: flex;
  padding: 0 0.5rem;
  color: ${colours.primary};
  background: ${lighten(0.1, colours.black)};
`;

export const ContentContainer = styled.div`
  padding: 1rem;
  flex-grow: 1;
  background: ${colours.black};
`;
