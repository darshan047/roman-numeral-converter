import React from 'react';
import ReactDOM from 'react-dom';
import 'jest-enzyme';
import { mount } from 'enzyme';
import App from './App';
import { convert } from './services/converter';

const flushAllPromises = () => new Promise(resolve => setImmediate(resolve));

export const flushRequestsAndUpdate = async enzymeWrapper => {
  await flushAllPromises();
  enzymeWrapper.update();
};

jest.mock('./services/converter', () => ({
  convert: jest.fn()
}));

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders input field with place holder text', async () => {
  const app = mount(<App />);
  const inputField = app.find('input[data-test="value-input-field"]');

  expect(inputField).toHaveLength(1);
  expect(inputField.props().placeholder).toEqual(
    'Enter either a numeric value (between 1 and 3999) or a set of roman numerals.'
  );
});

it('renders api response correctly', async done => {
  const mockResponse = {
    value: 'CXXIII',
    format: 'roman numeral'
  };

  convert.mockImplementation(() => {
    return Promise.resolve(mockResponse);
  });

  const app = mount(<App />);
  app
    .find('input[data-test="value-input-field"]')
    .simulate('keyup', { target: { value: '123' } });

  setTimeout(async () => {
    await flushRequestsAndUpdate(app);
    expect(app.find('h3[data-test="converted-value"]')).toHaveText(
      mockResponse.value
    );
    done();
  }, 1500);
});

it('renders error text when api returns error response', async done => {
  const mockResponse = {
    message: 'value is not a valid roman numeral'
  };

  convert.mockImplementation(() => {
    return Promise.reject(mockResponse);
  });
  const app = mount(<App />);

  const query = 'abcd';

  app
    .find('input[data-test="value-input-field"]')
    .simulate('keyup', { target: { value: query } });

  setTimeout(async () => {
    await flushRequestsAndUpdate(app);
    expect(app.find('p[data-test="message-desc"]')).toHaveText(
      mockResponse.message
    );
    done();
  }, 1500);
});
