import React from 'react';
import { string } from 'prop-types';
import { Container } from './styles';
import { H3, Text } from '../../globals/typography';

export default function Result({
  originalValue,
  originalFormat,
  convertedValue,
  convertedFormat
}) {
  return (
    <Container>
      <Text data-test="result-desc">{`${originalFormat.charAt(0).toUpperCase() +
        originalFormat.slice(
          1
        )} ${originalValue}  in ${convertedFormat}`}</Text>
      <H3 data-test="converted-value">{convertedValue}</H3>
    </Container>
  );
}

Result.propTypes = {
  originalValue: string.isRequired,
  originalFormat: string.isRequired,
  convertedValue: string.isRequired,
  convertedFormat: string.isRequired
};
