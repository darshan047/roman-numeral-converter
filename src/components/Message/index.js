import React from 'react';
import { string } from 'prop-types';
import { Container } from './styles';
import { H3, Text } from '../../globals/typography';

export default function Message({ title, decriptions }) {
  return (
    <Container>
      <H3 data-test="message-title">{title}</H3>
      <Text data-test="message-desc">{decriptions}</Text>
    </Container>
  );
}

Message.propTypes = {
  title: string.isRequired,
  description: string.isRequired
};

Message.defaultProps = {
  title: '',
  description: ''
};
