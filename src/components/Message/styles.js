import styled from 'styled-components';
import colours from '../../globals/colours';

export const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${colours.textLight};
`;
