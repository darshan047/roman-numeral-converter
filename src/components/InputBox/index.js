import React from 'react';
import { func } from 'prop-types';
import { Container, InputField } from './styles';

export default function InputBox({ onChange }) {
  const handleChange = event => {
    onChange(event.target.value);
  };
  return (
    <Container>
      <form>
        <InputField
          placeholder="Enter either a numeric value (between 1 and 3999) or a set of roman numerals."
          onKeyUp={handleChange}
          data-test="value-input-field"
          required
        />
      </form>
    </Container>
  );
}

InputBox.propTypes = {
  onChange: func
};
