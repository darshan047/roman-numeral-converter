import styled from 'styled-components';
import colours from '../../globals/colours';

export const Container = styled.div`
  flex: 1;
  color: ${colours.textLight};
`;

export const InputField = styled.input`
  width: 100%;
  border: none;
  padding: 1rem;
  font-size: 1.5rem;
  line-height: 1.5rem;
  font-weight: bold;
  caret-color: ${colours.primary};
  background-color: transparent;
  color: inherit;

  &:focus {
    outline: 0;
  }
`;
