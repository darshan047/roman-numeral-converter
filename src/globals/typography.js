import styled from 'styled-components';

export const H1 = styled.h1`
  font-size: 4.209rem;
  line-height: 1.15;
  margin-bottom: 1.5rem;
`;

export const H2 = styled.h2`
  ffont-size: 3.157rem;
  line-height: 1.15;
  margin-bottom: 1rem;
`;

export const H3 = styled.h3`
  font-size: 2.369rem;
  line-height: 1.15;
  margin-bottom: 1rem;
`;

export const H4 = styled.h4`
  font-size: 1.777rem;
  line-height: 1.15;
  margin-bottom: 0.5rem;
`;

export const H5 = styled.h5`
  font-size: 1.333rem;
  line-height: 1.15;
  margin-bottom: 0.5rem;
`;

export const Text = styled.p`
  font-size: 1rem;
  line-height: 1.15;
  margin-bottom: 0.5rem;
`;

export const TextSmall = styled.p`
  font-size: 0.75em;
  line-height: 1.15;
  margin-bottom: 0.5rem;
`;
