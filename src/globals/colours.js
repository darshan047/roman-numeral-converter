export default {
  primary: '#4ECDC4',
  pastelRed: '#FF6B6B',
  textDark: '#212121',
  textLight: '#FFF',
  white: '#FFF',
  black: '#333333'
};
