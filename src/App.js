import React from 'react';
import { useState, useEffect } from 'react';
import { debounce } from 'lodash';
import { convert } from './services';
import { Container, HeaderContainer, ContentContainer } from './styles';
import { InputBox, Result, Message } from './components';
import { ReactComponent as Logo } from './logo.svg';

function App() {
  const [value, setValue] = useState('');
  const [result, setResult] = useState({
    value: '',
    format: ''
  });
  const [error, setError] = useState('');

  const handleChange = value => {
    setValueDebounced(value);
  };

  const setValueDebounced = debounce(setValue, 1000);

  useEffect(() => {
    if (value === '') return;
    setResult({
      value: '',
      format: ''
    });
    setError('');
    let didCancel = false;
    async function getData() {
      try {
        const response = await convert({ value });
        const { value: convertedValue, format } = response;

        if (!didCancel) {
          setResult({ value: convertedValue, format });
        }
      } catch (err) {
        setError(err.message);
      }
    }

    getData();
    return () => {
      didCancel = true;
    };
  }, [value]);

  return (
    <Container>
      <HeaderContainer>
        <Logo width="3rem" colour="red" />
        <InputBox onChange={handleChange} />
      </HeaderContainer>

      <ContentContainer>
        {error && <Message title="Error" decriptions={error} />}
        {result.value && (
          <Result
            convertedValue={result.value}
            convertedFormat={result.format}
            originalValue={value}
            originalFormat={
              result.format === 'roman numeral' ? 'number' : 'roman numeral'
            }
          />
        )}
      </ContentContainer>
    </Container>
  );
}

export default App;
