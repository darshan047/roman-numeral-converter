import { get, getQueryString } from './helpers';

const basePath = '/api/converter';

export async function convert(queryObj) {
  let path = basePath;
  if (queryObj) {
    const queryString = getQueryString(queryObj);
    path = `${basePath}?${queryString}`;
  }

  return get(path);
}
