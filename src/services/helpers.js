export function get(url) {
  return fetch(url).then(handleResponse);
}

export async function handleResponse(response) {
  const data = await response.json();
  if (!response.ok) {
    throw data;
  }
  return data;
}

export function getQueryString(queryObj = {}) {
  let searchParams = new URLSearchParams();
  for (let key of Object.keys(queryObj)) {
    let value = queryObj[key];
    searchParams.append(key, value);
  }
  return searchParams.toString();
}
