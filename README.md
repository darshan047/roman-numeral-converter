## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all the application dependencies

### `npm start`

Runs the app (client and server) in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm start:server`

Runs the server in the development mode.<br>

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `Technologies \ Libraries used`

1. React Hooks - Avoided using Class based React components and using only Functional components with react hooks. Hooks are easier to work with and to test and make the code look cleaner, easier to read

2. State management - Becasue this is a trivial application, I'm storing the states in component's local state using react useState hook. When the applicaion gets more complex state should be moved to a central store

3. Styling - Using CSS in JS solution for styling. Styled Components help keep the concerns of styling and element architecture separated and make components more readable

4. Testing - Using Jest and Enzyme for unit / integration test react components. The biggest advantage of using Jest is that it works out of the box with minimal setup

5. Api - Using exprss js for creating the api endpoint becase it is easy to configure and customize

### `Directory structure`

- **converter**
  - [README.md](converter/README.md)
  - [jest.config.js](converter/jest.config.js)
  - [package-lock.json](converter/package-lock.json)
  - [package.json](converter/package.json)
  - **public**
    - [index.html](converter/public/index.html)
    - [manifest.json](converter/public/manifest.json)
  - **src**
    - [App.js](converter/src/App.js) - Main entry for client application
    - [App.test.js](converter/src/App.test.js) - Client side unit tests
    - **components** - Holds reuseable react components
      - **InputBox**
        - [index.js](converter/src/components/InputBox/index.js)
        - [styles.js](converter/src/components/InputBox/styles.js)
      - **Message**
        - [index.js](converter/src/components/Message/index.js)
        - [styles.js](converter/src/components/Message/styles.js)
      - **Result**
        - [index.js](converter/src/components/Result/index.js)
        - [styles.js](converter/src/components/Result/styles.js)
      - [index.js](converter/src/components/index.js)
    - **globals** - Client side global design tokens
      - [colours.js](converter/src/globals/colours.js)
      - [typography.js](converter/src/globals/typography.js)
    - [index.js](converter/src/index.js)
    - [logo.svg](converter/src/logo.svg)
    - [index.css](converter/src/index.css)
    - [serviceWorker.js](converter/src/serviceWorker.js)
    - [setupTests.js](converter/src/setupTests.js)
    - **services** - Api services and helpers
      - [helpers.js](converter/src/services/helpers.js)
      - [converter.js](converter/src/services/converter.js)
      - [index.js](converter/src/services/index.js)
    - [styles.js](converter/src/styles.js)
  - **server** - Server side code
    - **api**
      - **converter**
        - [fromRoman.js](converter/server/api/converter/fromRoman.js)
        - [index.js](converter/server/api/converter/index.js)
        - [toRoman.js](converter/server/api/converter/toRoman.js)
    - [index.js](converter/server/index.js) - Main entry for server application

### `Possible Improvements`

1. Client side validation - this will avoid unnecessary api calls
2. Loading spinner - to let them know things are working in the background
3. Detailed result - explantion of the conversion
4. Client side caching/memoization - this will reduce no. of api calls
5. Server side caching/memoization - this will avoid unnecessary repeated computation
6. Server side unit/integration testing - currently no testing added to the server side code
7. E2E testing for the Application
